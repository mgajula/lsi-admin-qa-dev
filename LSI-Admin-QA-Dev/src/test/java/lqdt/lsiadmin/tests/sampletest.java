package lqdt.lsiadmin.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.*;

import lqdt.lsiadmin.pages.LoginPage;
import lqdt.lsiadmin.pages.cgi_bin.AdminPage;
import lqdt.lsiadmin.pages.cgi_bin.CreateTaskPage;
import lqdt.lsiadmin.pages.cgi_bin.InventoryPage;
import lqdt.lsiadmin.pages.cgi_bin.PreferencesPage;
import lqdt.qa.common.*;
import lqdt.qa.*;


public class sampletest extends TestBase{
	
	
	@Test
	public void KnownUserLogin(){
		
		LoginPage loginPage = new LoginPage(driver);
		AdminPage adminPage = loginPage.Login(prop);
		
		CreateTaskPage createTaskPage = new CreateTaskPage(driver);
		AssertJUnit.assertTrue(driver.getPageSource().contains("Create Task")); 
		createTaskPage.CreateTask(prop);
		//Add Assertion to check TaskID link is displayed.
		//AssertJUnit.assertTrue(driver.getPageSource().contains()); 
		
		InventoryPage taskReady = new InventoryPage(driver);
		
		taskReady.actAcquisition();
		taskReady.readyWarehouse();
		taskReady.taskIDState();
		//Assertion to check WMS state is ready.
		//AssertJUnit.assertTrue(driver.getPageSource().contains("READY")); 
		
	}
}
