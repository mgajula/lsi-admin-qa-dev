package lqdt.lsiadmin.pages;

import java.util.Properties;

import lqdt.qa.common.*; 
import lqdt.lsiadmin.pages.cgi_bin.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

public class UploadManifestPage extends PageBase{
	
	public UploadManifestPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	private WebElement approve_button;//Button: Approve upload for task 
	
	public UploadManifestPage approveUpload(){
		
		approve_button.click();
		logger.info("Clicked on Approve upload for task button on Upload Manifest screen");
		
		return this;
		
	}
}
