package lqdt.lsiadmin.pages.cgi_bin;

import java.util.Properties;

import lqdt.qa.common.*; 
import lqdt.lsiadmin.pages.cgi_bin.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

public class BatchedWebRequestPage extends PageBase {

	public BatchedWebRequestPage(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.LINK_TEXT, using="View Results")
	private WebElement viewResults;//View Results link
	
	public BatchedWebRequestPage WebRequest(){
		
		//helper.wait(40);
		return this;
		
	}
	
	public BatchedWebRequestPage ReadyState(){
		
		//helper.wait(40);//Wait for 40 seconds to finish the job and view results to display
		//-- click on the View Results Link
		return this;
		
	}
	
	
}
