package lqdt.lsiadmin.pages.cgi_bin;


import lqdt.qa.common.*;
import lqdt.qa.*;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class InventoryPage extends PageBase {

	public InventoryPage(WebDriver driver) {
		super(driver);//Required
		//need to wrap navigation in if assertion
				PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.PARTIAL_LINK_TEXT, using="Task")
	private WebElement taskIdLink;//Task ID Link is displayed
	@FindBy(how=How.CSS, using="input[value='Import Items']")
	private WebElement importItem;//Import Item button
	@FindBy(how=How.CSS, using="input[value='Activate Acquisition']")
	private WebElement activateAcquisition;//Activate Acquisition button
	@FindBy(how=How.CSS, using="input[value='Ready For Warehouse Processing']")
	private WebElement warehouseProcessing;//Ready For Warehouse Processing button
	@FindBy(how=How.NAME, using="submit_comment")
	private WebElement addComment;//Ready For Warehouse Processing button
	
	public InventoryPage elements(){
		//Click on Import Items button
		importItem.click();
		logger.info("Import Items button clicked");
		
		return this;	
	}
	
	public InventoryPage actAcquisition(){
		
		taskIdLink.getText();
		System.out.println("Printing value of TaskID" + taskIdLink);
		activateAcquisition.click();//Click on Activate Acquisition button		
		logger.info("Clicked Activate Acquisition button");
		
		return this;	
	}
	
	public InventoryPage readyWarehouse(){
		
		boolean READY = false;
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(warehouseProcessing));
		
		warehouseProcessing.click();//Click on Ready For Warehouse Processing button
		Helper.Wait(10);
		logger.info("Clicked Ready For Warehouse Processing button");
		
		return this;	
	}
	
	
	public InventoryPage taskIDState(){
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(addComment));
		
		taskIdLink.click();//Click on TaskID link
		logger.info("Clicked TaskID link");	
		taskIdLink.click();
		logger.info("Agian clicked on TaskID link");
		
		return this;	
	}
	
}//class
