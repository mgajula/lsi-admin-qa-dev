package lqdt.lsiadmin.pages.cgi_bin;


import java.util.Properties;

import lqdt.qa.common.*; 
import lqdt.lsiadmin.pages.cgi_bin.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class CreateTaskPage extends PageBase{
	
	public CreateTaskPage(WebDriver driver){
		super(driver);
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getLsiUrl() + "/cgi-bin/inventory?_action=create&_table=task");
		PageFactory.initElements(driver, this);
	}
	
	private WebElement user_id;//Seller drop down
	private WebElement lpid_manifest_format_id;// LPID Format drop down
	@FindBy(how=How.NAME, using="warehouse_id")
	private WebElement warehouse;// Warehouse drop down
	@FindBy(how=How.NAME, using="purchase_amount")
	private WebElement purchaseAmount;//Text box for purchase amount
	@FindBy(how=How.NAME, using="submit")//Submit button
	private WebElement submit;
	
	
	public CreateTaskPage CreateTask(Properties prop){
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(submit));
		
		Select sellerValue = new Select(user_id);
		sellerValue.selectByVisibleText(prop.getProperty("lsiadmin.ctseller.value"));
		Helper.Wait(5);
		Select lpidFormatValue = new Select(lpid_manifest_format_id);
		lpidFormatValue.selectByVisibleText(prop.getProperty("lsiadmin.lpidformat.value"));
		
		Select warehouseValue = new Select(warehouse);
		warehouseValue.selectByVisibleText(prop.getProperty("lsiadmin.warehouse.value"));
		logger.info("Selecting values on create task screen is completed");
		
		submit.click();
		logger.info("Submit button clicked on CreateTask Page");
		Helper.Wait(20);
		
		return this;
		
		
	}
	
	
}//End Class