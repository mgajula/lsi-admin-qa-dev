package lqdt.lsiadmin.pages.cgi_bin;


import java.util.Properties;

import lqdt.qa.common.*; 
import lqdt.lsiadmin.pages.cgi_bin.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

public class PreferencesPage extends PageBase{
	
	public PreferencesPage(WebDriver driver){
		super(driver);
	
		driver.navigate().to(Global.getLsiUrl() + "/cgi-bin/inventory?_action=preferences&_table=task");
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.NAME, using="use_frames")
	private WebElement useFrames;// User frames drop down
	private WebElement frameValue;
	@FindBy(how=How.CSS, using="input[value='submit']")
	private WebElement submit;//Submit button

	public PreferencesPage noFrame(Properties prop){
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(submit));//Wait untill Submit button is visible on preferences screen.
		
		Select frameOption = new Select(frameValue);
		frameOption.selectByVisibleText(prop.getProperty("lsiadmin.noframesettings.value"));
		submit.click();
		logger.info("Clicked on Submit button on preference page");
		
		return this;
		
	}
	
	public PreferencesPage yesFrame(Properties prop){
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(submit));//Wait untill Submit button is visible on preferences screen.
		
		Select frameOption = new Select(frameValue);
		frameOption.selectByVisibleText(prop.getProperty("lsiadmin.yesframesettings.value"));
		submit.click();
		logger.info("Clicked on Submit button on preference page");
		
		return this;
		
	}
	
}
