package lqdt.lsiadmin.pages.cgi_bin;

import java.util.Properties;

import lqdt.qa.common.*;
import lqdt.qa.*;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class AdminPage extends PageBase {

	public AdminPage(WebDriver driver) {
		super(driver);//Required
		
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getLsiUrl() + "/cgi-bin/admin");
		PageFactory.initElements(driver, this);
	}
	

//	Actions action = new Actions(driver);
//	WebElement we = driver.findElement(By.id("user_menuitem"));//User mouse over 
//	action.moveToElement(we).build().perform();
//	System.out.println("Moved to menu item");
//	//Thread.sleep(3000);
//	//V3 Users
//	driver.findElement(By.linkText("V3 Users")).click();
//	System.out.println("Clicked on V3 Users");
//	Thread.sleep(3000);

}//class
