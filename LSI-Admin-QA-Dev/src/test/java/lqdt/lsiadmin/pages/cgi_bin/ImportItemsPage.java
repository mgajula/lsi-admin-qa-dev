package lqdt.lsiadmin.pages.cgi_bin;

import java.util.Properties;

import lqdt.qa.common.*; 
import lqdt.lsiadmin.pages.cgi_bin.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class ImportItemsPage extends PageBase{

	public ImportItemsPage (WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="edited_file")
	private WebElement editedFile;
	@FindBy(how=How.CSS, using="button[id='submit_button']")
	private WebElement uploadFiles;
	
	
	public  ImportItemsPage importItems(Properties prop){
	
		//logger.info("Browse and upload manifest file");
		
		return this;
		
	}
}
